package io.adavliatov.task.binomo.broker;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class Message {

    public final Integer id;
    public final String message;

    @JsonCreator
    public Message(@JsonProperty("id") Integer id,
                   @JsonProperty("message") String message) {
        this.id = id;
        this.message = message;
    }

    @Override
    public String toString() {
        return "SampleMessage{id=" + this.id + ", message='" + this.message + "'}";
    }

}