package io.adavliatov.task.binomo.broker;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "cloudkarafka")
@Validated
public class CloudkarafkaProperties {

    @Value("topic")
    public String topic;
}
